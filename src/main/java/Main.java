
import dtos.Group;
import dtos.Student;
import services.Persistence;

public class Main {
    public static void main(String[] args) {
        Group group1 = new Group(1, "group 1", 5);
        Group group2 = new Group(2, "group 2", 3);
        Student student1 = new Student(101, "Петров Иван Васильевич", "Москва", group1);
        Student student2 = new Student(102, "Иванов Василий Петрович", "Москва", group1);
        Student student3 = new Student(103, "Васильев Петр Иванович", "Москва", group1);
        Student student4 = new Student(104, "Сидоров Сергей Владимирович", "Екатеринбург", group2);
        Student student5 = new Student(105, "Сергеев Владимир Николаевич", "Екатеринбург", group2);

        Persistence persistence = new Persistence();
        persistence.save(student1);
        persistence.save(student2);
        persistence.save(student3);
        persistence.save(student4);
        persistence.saveWithGroup(student5);
        System.out.println(persistence.get(101));
        System.out.println(persistence.get(102));
        System.out.println(persistence.get(103));
        System.out.println(persistence.get(104));
        System.out.println(persistence.get(105));
    }
}
