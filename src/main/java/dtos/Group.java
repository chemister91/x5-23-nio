package dtos;

import java.io.Serializable;
import java.util.Objects;

public class Group implements Serializable {
    private Integer id;
    private String name;
    private Integer graduationYear;

    public Group() {
    }

    public Group(Integer id, String name, Integer graduationYear) {
        this.id = id;
        this.name = name;
        this.graduationYear = graduationYear;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", graduationYear=" + graduationYear +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        Group group = (Group) o;
        return Objects.equals(getId(), group.getId()) &&
                Objects.equals(getName(), group.getName()) &&
                Objects.equals(getGraduationYear(), group.getGraduationYear());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getGraduationYear());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(Integer graduationYear) {
        this.graduationYear = graduationYear;
    }
}
