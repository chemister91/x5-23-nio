package dtos;

import java.io.Serializable;
import java.util.Objects;

public class Student implements Serializable {
    private Integer id;
    private String fio;
    private String city;
    Group group;

    public Student() {
    }

    public Student(Integer id, String fio, String city, Group group) {
        this.id = id;
        this.fio = fio;
        this.city = city;
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", fio='" + fio + '\'' +
                ", city='" + city + '\'' +
                ", group=" + group.toString() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return Objects.equals(getId(), student.getId()) &&
                Objects.equals(getFio(), student.getFio()) &&
                Objects.equals(getCity(), student.getCity()) &&
                Objects.equals(getGroup(), student.getGroup());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFio(), getCity(), getGroup());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
