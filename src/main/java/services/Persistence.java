package services;

import dao.JSONDAO;
import dtos.Student;

public class Persistence {
    private static final String FILE_PATTERN = "student-%d.sv";
    private static final String PATH_PATTERN = "group-%d";
    private JSONDAO jsondao;

    public Persistence() {
        jsondao = new JSONDAO();
    }

    public Persistence(JSONDAO jsondao) {
        this.jsondao = jsondao;
    }

    public boolean save(Student student) {
        return jsondao.toJSONFile(student, String.format(FILE_PATTERN, student.getId()));
    }

    public boolean saveWithGroup(Student student) {
        String fileName = String.format(FILE_PATTERN, student.getId());
        String path = String.format(PATH_PATTERN, student.getGroup().getId());
        return jsondao.toJSONFile(student, path, String.format("%s/%s", path, fileName));
    }

    public Student get(Integer id) {
        return jsondao.fromJSONFile(String.format(FILE_PATTERN, id), Student.class);
    }


}
