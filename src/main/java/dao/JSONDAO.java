package dao;

import com.google.gson.Gson;
import com.google.gson.internal.Primitives;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class JSONDAO {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private RandomAccessFile randomAccessFile;
    private ByteBuffer byteBuffer;
    private FileChannel fileChannel;

    public JSONDAO() {
    }

    public JSONDAO(RandomAccessFile randomAccessFile, FileChannel fileChannel, ByteBuffer byteBuffer) {
        this.randomAccessFile = randomAccessFile;
        this.byteBuffer = byteBuffer;
        this.fileChannel = fileChannel;
    }

    private RandomAccessFile getGetRandomAccessFile(String fileName) throws FileNotFoundException {
        if (randomAccessFile == null) {
            return new RandomAccessFile(fileName, "rw");
        } else {
            return randomAccessFile;
        }
    }

    private FileChannel getFileChannel(RandomAccessFile aFile) {
        if (fileChannel == null) {
            return aFile.getChannel();
        } else {
            return fileChannel;
        }
    }

    private ByteBuffer getByteBuffer() {
        if (byteBuffer == null) {
            return ByteBuffer.allocate(512);
        } else {
            return byteBuffer;
        }
    }

    public boolean toJSONFile(Object object, String path, String fileName) {
        try {
            if (randomAccessFile == null) {
                Files.createDirectories(Paths.get(path));
            }
            return toJSONFile(object, fileName);
        } catch (IOException e) {
            logger.severe(e.toString());
            return false;
        }
    }

    public boolean toJSONFile(Object object, String fileName) {
        try {
            String json = new Gson().toJson(object);
            try (RandomAccessFile aFile = getGetRandomAccessFile(fileName)) {
                return toFile(getFileChannel(aFile), getByteBuffer(), json);
            }
        } catch (Exception e) {
            logger.severe(e.toString());
            return false;
        }
    }

    public boolean toFile(FileChannel channel, ByteBuffer buffer, String text) {
        try {
            buffer.clear();
            buffer.put(text.getBytes());
            buffer.flip();
            while (buffer.hasRemaining()) {
                if (channel.write(buffer) == -1) {
                    break;
                }
            }
            return true;
        } catch (IOException e) {
            logger.severe(e.toString());
            return false;
        }
    }

    public <T> T fromJSONFile(String fileName, Class<T> classOfT) {
        try {
            String json;
            try (RandomAccessFile aFile = getGetRandomAccessFile(fileName)) {
                json = fromFile(getFileChannel(aFile), getByteBuffer());
            }
            return new Gson().fromJson(json, classOfT);
        } catch (IOException e) {
            logger.severe(e.toString());
            return Primitives.wrap(classOfT).cast(new Object());
        }
    }

    public String fromFile(FileChannel channel, ByteBuffer buffer) {

        try {
            StringBuilder builder = new StringBuilder();
            Charset charset = Charset.forName("UTF-8");
            while (channel.read(buffer) != -1) {
                buffer.flip();
                while (buffer.hasRemaining()) {
                    builder.append(charset.decode(buffer));
                }
                buffer.clear();
            }
            return builder.toString();
        } catch (IOException e) {
            logger.severe(e.toString());
            return "";
        }
    }
}
