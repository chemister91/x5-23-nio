package services;

import dao.JSONDAO;
import dtos.Group;
import dtos.Student;
import org.junit.Test;
import org.junit.Before;
import org.mockito.Mock;


import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class PersistenceTest {
    private JSONDAO jsondao;
    private ByteBuffer byteBuffer;
    private Persistence persistence;

    @Mock
    RandomAccessFile randomAccessFile;
    @Mock
    FileChannel fileChannel;

    @Before
    public void setUp() throws IOException {
        initMocks(this);
        prepareForTest();
    }

    @Test
    public void fileNotExists() {
        assertNull(persistence.get(666));
    }

    @Test
    public void checkEquals() {
        assertEquals(getStudent(), persistence.get(any()));
    }

    @Test
    public void testWrite() {
        assertTrue(persistence.save(getStudent()));
        assertTrue(persistence.saveWithGroup(getStudent()));
    }

    private void prepareForTest() throws IOException {
        byteBuffer = ByteBuffer.allocate(512);
        jsondao = new JSONDAO(randomAccessFile, fileChannel, byteBuffer);
        persistence = new Persistence(jsondao);
        when(fileChannel.read(byteBuffer)).thenReturn(0).thenReturn(-1);
        when(fileChannel.write(byteBuffer)).thenReturn(0).thenReturn(-1);
    }

    private Student getStudent() {
        Group group1 = new Group(1, "group 1", 5);
        Student student = new Student(101, "Петров Иван Васильевич", "Москва", group1);
        jsondao.toJSONFile(student, "");
        byteBuffer.position(byteBuffer.limit());
        return student;
    }


}